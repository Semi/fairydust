from st3m.application import Application, ApplicationContext
from st3m import InputState
import st3m.run
import leds
import math

## Fairy dust application for the flow3r badge
##

class FairyDust(Application):
	def __init__(self, app_ctx: ApplicationContext) -> None:
		super().__init__(app_ctx)
		self._stage = 0
		self._fire = 5
		self._oldfire = 5
		self._direction = 0
		self._angles = [0, 0.63, 1.26, 1.88, 2.51, 3.14, 3.77, 4.40, 5.03, 5.65]
		
	def draw(self, ctx: Context) -> None:
		# Paint the background black
		ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

		# Rotate ctx in direction of active petal
		ctx.rotate(self._angles[self._direction])

		# Paint the foreground stars
		flanes = range(-200, 240, 40)
		for c, lane in enumerate(flanes):
			diff = lane * ((-1) ** (c + 1)) # alternate and spread starting postions
			size = (((c % 2) + 1) * 2) + 2
			ctx.rgb(255, 255, 255).rectangle(lane, ((self._stage % 25) * 10) - diff, size, size).fill()
			ctx.rgb(255, 255, 255).rectangle(lane, ((self._stage % 25) * 10) - diff - 250, size, size).fill()
			
		# Paint the background stars
		blanes = range(-200, 240, 20)
		for c, lane in enumerate(blanes):
			diff = lane * ((-1) ** (c + 1)) # alternate and spread starting postions
			size = (c % 2) + 1
			ctx.rgb(100, 100, 100).rectangle(lane, (self._stage * size) - diff, size, size).fill()
			ctx.rgb(100, 100, 100).rectangle(lane, (self._stage * size) - diff - 250, size, size).fill()
			ctx.rgb(100, 100, 100).rectangle(lane, (self._stage * size) - diff - 500, size, size).fill()
		 
		# Display rocket according to the petal direction
		# Rotate ctx back to 0
		ctx.rotate(-(self._angles[self._direction]))
		ctx.image(f"{self.app_ctx.bundle_path}/img/fairy_dust_" + str(self._direction) + "_0.png", -120, -120, 240, 240)
		
		# LED stars
		for l in range(0, 40, 2):
			leds.set_rgb(l, 50, 50, 50)

		# LED fire
		# If fire direction has changed, we must clean up
		if self._fire != self._oldfire:
			cleds = list()
			for l in range(4):
				cleds.append((self._oldfire * 4) + l)
				nled = (self._oldfire * 4) - l
				if nled < 0:
					nled = 40 + nled
				cleds.append(nled)

			# Turn all possibly firing leds off
			for l in cleds:
				leds.set_rgb(l, 0, 0, 0)
			
			self._oldfire = self._fire
			
		
		# Find all possible firing leds
		fleds = list()
		for l in range(4):
			fleds.append((self._fire * 4) + l)
			nled = (self._fire * 4) - l
			if nled < 0:
				nled = 40 + nled
			fleds.append(nled)

		# Turn all possible firing leds off
		for l in fleds:
			leds.set_rgb(l, 0, 0, 0)

		# Find the actually firing leds
		flons = list()
		
		if self._stage % 4 == 0:
			if self._fire % 2 != 0:
				flons.append(self._fire * 4)
			else:
				led1 = (self._fire * 4) - 4
				if led1 < 0:
					led1 = 40 + led1
				flons.append(led1)
				flons.append((self._fire * 4) + 4)
				
		elif self._stage % 4 == 1:
			if self._fire % 2 != 0:
				flons.append((self._fire * 4) - 1)
				flons.append((self._fire * 4) + 1)
			else:
				led1 = (self._fire * 4) - 3
				if led1 < 0:
					led1 = 40 + led1
				flons.append(led1)
				flons.append((self._fire * 4) + 3)
		elif self._stage % 4 == 2:
			flons.append((self._fire * 4) - 2)
			flons.append((self._fire * 4) + 2)
		elif self._stage % 4 == 3:
			if self._fire % 2 != 0:
				flons.append((self._fire * 4) - 3)
				flons.append((self._fire * 4) + 3)
			else:
				led1 = (self._fire * 4) - 1
				if led1 < 0:
					led1 = 40 + led1
				flons.append(led1)
				flons.append((self._fire * 4) + 1)

		# FIRE!
		for l in flons:
			leds.set_rgb(l, 255, 102, 0)
			 
		leds.update()
		
		# Cycle stage of animation
		self._stage += 1
		if self._stage > 500:
			self._stage = 0
		
	def think(self, ins: InputState, delta_ms: int) -> None:
		super().think(ins, delta_ms) # Let Application do its thing
		
		# Read petal state
		for index, petal in enumerate(self.input.captouch.petals):
			if petal.whole.pressed:
				self._oldfire = self._fire
				self._direction = index
				if self._direction < 5:
					self._fire = self._direction + 5
				elif self._direction >= 5:
					self._fire = self._direction - 5

if __name__ == '__main__':
	st3m.run.run_view(FairyDust(ApplicationContext()))
